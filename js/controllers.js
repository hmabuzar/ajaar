var mainCtrl = angular.module("mainCtrl", []);

mainCtrl.controller("genCtrl", function ($scope, $rootScope, $window, $state, $timeout, $location, $stateParams, $document, apiService, toaster, Auth) {
    $scope.logout = function () {
        $scope.loader = true;
        apiService.logout.get().$promise.then(function (data) {
            if (data.status == "success") {
                $window.sessionStorage.removeItem("loggedIn");
                $window.sessionStorage.removeItem("token");
                $window.sessionStorage.setItem("contract_id", null);
                $state.go("login");
                $scope.loader = false;
            } else {
                toaster.pop("error", "Server Error");
                $scope.loader = false;
            }
        })
    };

    $scope.$on('$viewContentLoaded', function () {
        $('#accordion h3, .accordion-inner h3').click(function () {
            $(this).next().slideToggle();
            $(this).toggleClass("active");
            return false;
        }).next().hide();

        $("#start-date").datepicker({
            "dateFormat": "dd-mm-yy",
        });

        $("#end-date").datepicker({
            "dateFormat": "dd-mm-yy",
        });
    });

    $scope.countries = apiService.countries.get().$promise.then(function (res) {
        $scope.countries = res.countries;
    });

    apiService.loadterms.get().$promise.then(function (response) {
        $scope.sections = response.data;
        $scope.fixed = function (item) {
            return item.optional == "no";
        };
        $scope.optional = function (item) {
            return item.optional == "yes";
        };
    });

    $scope.scrollTop = function () {
        $document.scrollTo(0, 0, [1000]);
    };

    $scope.forgotPass = {};
    $scope.forgotpass = function () {
        $scope.loader = true;
        var params = {
            forgotpass_email: $scope.forgotPass.email,
        }
        apiService.forgotpass.get(params).$promise.then(function (res) {
            $scope.loader = false;
            if (res.status == "Success") {
                $scope.emailSent = true;
                $scope.forgotPass.email = null;
            } else if (res.status == "Error") {
                $scope.emailNotSent = true;
            }
        })
    };
    $scope.resetPassForm = {};
    $scope.resetpass = function () {
        var email = $location.search().token;
        $scope.loader = true;
        var params = {
            email: $location.search().token,
            resetpass_password: $scope.resetPassForm.npassword,
            //resetpass_cpassword		:	$scope.resetPassForm.cpassword
        }
        apiService.resetpass.get(params).$promise.then(function (res) {
            $scope.loader = false;
            if (res.status == "success") {
                $state.go("login");
            } else {
                toaster.pop("error", "Server Error");
            }
        })
    }

    $scope.contactForm = {};
    $scope.contactus = function () {
        $scope.loader = true;
        var params = {
            full_name: $scope.contactForm.fullName,
            email: $scope.contactForm.emailMode,
            phone: $scope.contactForm.phone,
            msg: $scope.contactForm.message
        }
        apiService.contactus.save(params).$promise.then(function (res) {
            if (res.status == "Success") {
                $scope.loader = false;
                toaster.pop("success", "Email Sent");
                $scope.contactForm.fullName = null;
                $scope.contactForm.emailMode = null;
                $scope.contactForm.phone = null;
                $scope.contactForm.message = null;
            } else {
                toaster.pop("error", "Server Error");
            }
        })
    }

});

mainCtrl.controller("loginCtrl", function ($scope, $rootScope, $window, $state, toaster, apiService, Auth) {
    $scope.formData = {};
    $scope.login = function () {
        $scope.loader = true;
        var params = {
            email: $scope.formData.email,
            password: $scope.formData.password
        }
        apiService.login.get(params).$promise.then(function (data) {
            if (data.msg == "Not verified") {
                $scope.verify = true;
                $scope.loader = false;
            } else {
                $scope.verify = false;
                if (data.status == "success") {
                    $window.sessionStorage.setItem("loggedIn", true);
                    $window.sessionStorage.setItem("token", data.token);
                    $scope.loader = false;
                    $state.go("contracts");
                } else {
                    $scope.invalid = true;
                    $scope.loader = false;
                }
            }
        });
    }
});

mainCtrl.controller("regCtrl", function ($scope, $rootScope, apiService, toaster, $state) {
    $scope.regData = {};
    $scope.register = function () {
        $scope.loader = true;
        $scope.submitted = true;
        var params = {
            first_name: $scope.regData.firstname,
            last_name: $scope.regData.lastname,
            email: $scope.regData.email,
            password: $scope.regData.password,
            mobile: $scope.regData.mobile
        }
        apiService.register.get(params).$promise.then(function (data) {
            if (data.status == "success") {
                toaster.pop("success", "New User Registered")
                $scope.loader = false;
                $state.go("login");
            } else {
                toaster.pop("error", data.msg);
                $scope.loader = false;
            }
        });
    }
});

mainCtrl.controller("contracts", function ($scope, $state, $rootScope, $filter, toaster, $location, $stateParams, $window, apiService) {
    $scope.loader = true;
    var tokenParams = {
        token: $window.sessionStorage.getItem("token"),
        reqTime: new Date().getTime()
    }
    apiService.contracts.query(tokenParams).$promise.then(function (res) {
        $scope.loader = false;
        if (res.msg == "No record found") {
            $scope.noRecord = true;
            $scope.contracts = res.contracts;
        } else {
            $scope.contracts = res.contracts;
        }
    });

    $scope.delContract = function (id) {
        $scope.loader = true;
        var params = {
            token: $window.sessionStorage.getItem("token"),
            contract_id: id
        }
        apiService.contractdetails.delete(params).$promise.then(function (res) {
            if (res.status == "Success") {
                toaster.pop("success", "Deleted");
                if (res.contract_list !== null) {
                    $scope.loader = false;
                    $scope.contracts = res.contracts_list.contracts;
                } else {
                    $scope.noRecord = true;
                    $scope.loader = false;
                }
            } else if (res.status == "Error") {
                toaster.pop("error", res.msg);
                $scope.loader = false;
            }
        })
    }
});

mainCtrl.controller("contractdetails", function ($scope, $state, $rootScope, toaster, $stateParams, $window, apiService) {
    $scope.loader = true;
    var detailsParams = {
        reqTime: new Date().getTime(),
        contract_id: $stateParams.id,
        token: $window.sessionStorage.getItem("token")
    }
    apiService.contractdetails.get(detailsParams).$promise.then(function (res) {
        $scope.loader = false;
        $scope.contractDetails = res.contracts;
        $scope.sections = res.contracts[0].tnc;
        $scope.fixed = function (item) {
            return item.optional == "no";
        };
        $scope.optional = function (item) {
            return item.optional == "yes";
        };
    })

    $scope.genPdf = function () {
        $scope.loader = true;
        var params = {
            "token": $window.sessionStorage.getItem("token"),
            "pdf": "yes",
            "contract_id": $stateParams.id
        }
        apiService.savecontract.save(params).$promise.then(function (res) {
            if (res.status == "Success") {
                $scope.loader = false;
                toaster.pop("success", "Pdf generated and email sent");
            } else if (res.status = "error") {
                $scope.loader = false;
                toaster.pop("error", "Server Error");
            }
        })
    };
});

mainCtrl.controller("editcontract", function ($scope, $state, $document, $rootScope, toaster, $stateParams, $window, apiService) {
    $scope.loader = true;
    $scope.tenantIndForm = {};
    $scope.tenantComForm = {};
    $scope.landlordIndForm = {};
    $scope.landlordComForm = {};
    $scope.propertyApaForm = {};
    $scope.propertyVilForm = {};
    $scope.tenancytForm = {};
    $scope.optionalTerms = [];
    $scope.toggleSelection = function toggleSelection(term) {
        var idx = $scope.optionalTerms.indexOf(term);
        if (idx > -1) {
            $scope.optionalTerms.splice(idx, 1);
        } else {
            $scope.optionalTerms.push(term);
        }
    }
    $scope.lang = {
        name: ' ',
    };
    $scope.tenantType = "individual";
    $scope.landlordType = "individual";
    $scope.propertyType = "apartment";
    $window.sessionStorage.setItem("landlord_id", "null");
    $window.sessionStorage.setItem("contract_id", $stateParams.id);
    var editParams = {
        reqTime: new Date().getTime(),
        contract_id: $stateParams.id,
        token: $window.sessionStorage.getItem("token")
    };
    apiService.contractdetails.get(editParams).$promise.then(function (res) {
        $scope.loader = false;
        $scope.editLandlord = false;
        var contDetails = res.contracts[0];
        $scope.contDetails = res.contracts;
        $scope.landlords = res.contracts[0].landlords;
        $scope.uploadedfiles = res.contracts[0].images;
        if ($scope.landlords) {
            angular.forEach($scope.landlords, function (value, index) {
                if (value.l_type == 'individual') {
                    if ($scope.landlords.length < 3) {
                        $scope.addAnotherLand = true;
                    }
                } else if (value.l_type == 'company') {
                    $scope.addAnotherLand = false;
                }
            })
        }
        if (!$scope.landlords) {
            $scope.addAnotherLand = true;
        }
        $scope.tenantType = contDetails.tenant_type;
        $scope.tenantIndForm.tFirstName = contDetails.first_name;
        $scope.tenantIndForm.tLastName = contDetails.last_name;
        $scope.tenantIndForm.tEmployeeName = contDetails.employee_name;
        $scope.tenantIndForm.tJobPosition = contDetails.job_position;
        $scope.tenantIndForm.tMobileNumber = contDetails.mobile;
        $scope.tenantIndForm.tEmailAddress = contDetails.email;
        $scope.tenantIndForm.tEmiratesId = contDetails.emirates_id;
        $scope.tenantIndForm.tNationality = contDetails.nationality;
        $scope.tenantIndForm.tPassportNumber = contDetails.passport_no;

        $scope.tenantComForm.tCompanyName = contDetails.company_name;
        $scope.tenantComForm.tEmiratesLicense = contDetails.emirates_license_no;
        $scope.tenantComForm.tEstCard = contDetails.establishment_card_no;
        $scope.tenantComForm.tIssuingAuthority = contDetails.issuing_authority;
        $scope.tenantComForm.tPoBox = contDetails.po_box;
        $scope.tenantComForm.tManagerFname = contDetails.manager_first_name;
        $scope.tenantComForm.tManagerLname = contDetails.manager_last_name;
        $scope.tenantComForm.tManagerEid = contDetails.manager_emirates_id_no;
        $scope.tenantComForm.tTelephoneNumber = contDetails.company_phone;
        $scope.tenantComForm.tManagerMnumber = contDetails.company_mobile;
        $scope.tenantComForm.tCompanyEmail = contDetails.company_email;

        $scope.propertyType = contDetails.property_type;
        $scope.propertyApaForm.aAlias = contDetails.a_alias;
        $scope.propertyApaForm.aTitleDeed = contDetails.a_deed_no;
        $scope.propertyApaForm.aDewaNumber = contDetails.a_dewa_no;
        $scope.propertyApaForm.aUnitType = contDetails.a_unit_type;
        $scope.propertyApaForm.aUnitNumber = contDetails.a_unit_no;
        $scope.propertyApaForm.aFloorNumber = contDetails.a_floor_no;
        $scope.propertyApaForm.aBuildingName = contDetails.a_building_name;
        $scope.propertyApaForm.aCommunityArea = contDetails.a_community_area;
        $scope.propertyApaForm.aCity = contDetails.a_city;
        $scope.propertyApaForm.aStreet = contDetails.a_street;
        $scope.propertyApaForm.aParkingLot = contDetails.parking_lot_no;
        $scope.propertyVilForm.vAlias = contDetails.v_alias;
        $scope.propertyVilForm.vTitleDeed = contDetails.v_deed_no;
        $scope.propertyVilForm.vDewaNumber = contDetails.v_dewa_no;
        $scope.propertyVilForm.vUnitType = contDetails.v_unit_type;
        $scope.propertyVilForm.vUnitNumber = contDetails.v_unit_no;
        $scope.propertyVilForm.vFloorNumber = contDetails.v_floor_no;
        $scope.propertyVilForm.vBuildingName = contDetails.v_building_name;
        $scope.propertyVilForm.vCommunityArea = contDetails.v_community_area;
        $scope.propertyVilForm.vCity = contDetails.v_city;
        $scope.propertyVilForm.vStreet = contDetails.v_street;

        $scope.tenancytForm.leaseDuration = contDetails.lease_duration;
        $scope.tenancytForm.startDate = contDetails.start_date;
        $scope.tenancytForm.endDate = contDetails.end_date;
        $scope.tenancytForm.rentValue = contDetails.rent_value;
        $scope.tenancytForm.securityDeposit = contDetails.security_deposit;
        $scope.tenancytForm.paymentTerms = contDetails.payment_terms;
        $scope.tenancytForm.numberPayment = contDetails.no_of_payments;
        $scope.tenancytForm.amountsDates = contDetails.amounts_dates;
    })

    $scope.addTenant = function () {
        $scope.loader = true;
        var params = {
            "token": $window.sessionStorage.getItem("token"),
            "contract_id": $window.sessionStorage.getItem("contract_id"),
            "tenant": {
                tenant_type: $scope.tenantType,
                t_first_name: $scope.tenantIndForm.tFirstName,
                t_last_name: $scope.tenantIndForm.tLastName,
                t_employee_name: $scope.tenantIndForm.tEmployeeName,
                t_job_position: $scope.tenantIndForm.tJobPosition,
                t_mobile_number: $scope.tenantIndForm.tMobileNumber,
                t_email_address: $scope.tenantIndForm.tEmailAddress,
                t_emirates_id: $scope.tenantIndForm.tEmiratesId,
                t_nationality: $scope.tenantIndForm.tNationality,
                t_passport_number: $scope.tenantIndForm.tPassportNumber,
                t_company_name: $scope.tenantComForm.tCompanyName,
                t_emirates_license: $scope.tenantComForm.tEmiratesLicense,
                t_est_card: $scope.tenantComForm.tEstCard,
                t_issuing_authority: $scope.tenantComForm.tIssuingAuthority,
                t_po_box: $scope.tenantComForm.tPoBox,
                t_manager_fname: $scope.tenantComForm.tManagerFname,
                t_manager_lname: $scope.tenantComForm.tManagerLname,
                t_telephone_number: $scope.tenantComForm.tTelephoneNumber,
                t_manager_mnumber: $scope.tenantComForm.tManagerMnumber,
                t_company_email: $scope.tenantComForm.tCompanyEmail,
                t_manager_eid: $scope.tenantComForm.tManagerEid
            }
        }
        apiService.savecontract.save(params).$promise.then(function (res) {
            if (res.status == "Success") {
                $scope.loader = false;
                toaster.pop("success", "Saved");
                var contract_id = res.contract_id;
                if (contract_id != undefined) {
                    $window.sessionStorage.setItem("contract_id", res.contract_id);
                }
            } else if (res.status = "error") {
                $scope.loader = false;
                toaster.pop("error", "Server error, please try later");
            }
        })
    }

    $scope.addLandlord = function () {
        $scope.loader = true;
        var params = {
            "token": $window.sessionStorage.getItem("token"),
            "contract_id": $window.sessionStorage.getItem("contract_id"),
            "landlord": {
                "landlord_id": $window.sessionStorage.getItem("landlord_id"),
                landlord_type: $scope.landlordType,
                first_name: $scope.landlordIndForm.lFirstName,
                last_name: $scope.landlordIndForm.lLastName,
                mobile_number: $scope.landlordIndForm.lMobileNumber,
                email_address: $scope.landlordIndForm.lEmailAddress,
                emirates_id: $scope.landlordIndForm.lEmiratesId,
                nationality: $scope.landlordIndForm.lNationality,
                passport_number: $scope.landlordIndForm.lPassportNumber,
                emirate_id_front: $scope.landlordIndForm.lFrontImage,
                emirate_id_back: $scope.landlordIndForm.lBackImage,
                company_name: $scope.landlordComForm.lCompanyName,
                emirates_license: $scope.landlordComForm.lEmiratesLicense,
                est_card: $scope.landlordComForm.lEstCard,
                issuing_authority: $scope.landlordComForm.lIssuingAuthority,
                po_box: $scope.landlordComForm.lPoBox,
                manager_fname: $scope.landlordComForm.lManagerFname,
                manager_lname: $scope.landlordComForm.lManagerLname,
                telephone_number: $scope.landlordComForm.lTelephoneNumber,
                manager_mnumber: $scope.landlordComForm.lManagerMnumber,
                company_email: $scope.landlordComForm.lCompanyEmail,
                manager_eid: $scope.landlordComForm.lManagerEid,
                company_license_image: $scope.landlordComForm.lCompanyLicenseImage
            }
        };
        apiService.landlord.save(params).$promise.then(function (res) {
            $scope.loader = false;
            if (res.status == "Success") {
                toaster.pop("success", "Saved");
                $scope.landlords = res.landlord;
                $window.sessionStorage.setItem("landlord_id", "null");
                if ($scope.landlordType == 'company') {
                    $scope.addAnotherLand = false;
                    $scope.noLandlordFound = false;
                    var formTop = angular.element($('.form-top'));
                    $document.scrollToElementAnimated(formTop, [2000]);
                } else {
                    $scope.addAnotherLand = true;
                    $scope.noLandlordFound = false;
                }
                $scope.landlordIndForm = {};
                $scope.landlordInd.$setUntouched();
                $scope.landlordInd.$setPristine();
                $scope.landlordComForm = {};
                $scope.landlordCom.$setUntouched();
                $scope.landlordCom.$setPristine();
                angular.forEach(angular.element("input[type='file']"), function (inputElem) {
                    angular.element(inputElem).val(null);
                });
            } else {
                toaster.pop("error", "Server error, please try later");
            }
            if (res.landlord) {
                if (res.landlord.length == 3) {
                    $scope.addAnotherLand = false;
                }
            }
            $document.scrollTo(0, 250, [1000]);
        });
    };

    $scope.addProperty = function () {
        $scope.loader = true;
        var params = {
            "token": $window.sessionStorage.getItem("token"),
            "contract_id": $window.sessionStorage.getItem("contract_id"),
            "property": {
                property_type: $scope.propertyType,
                a_alias: $scope.propertyApaForm.aAlias,
                a_title_deed: $scope.propertyApaForm.aTitleDeed,
                a_dewa_number: $scope.propertyApaForm.aDewaNumber,
                a_unit_type: $scope.propertyApaForm.aUnitType,
                a_unit_number: $scope.propertyApaForm.aUnitNumber,
                a_floor_number: $scope.propertyApaForm.aFloorNumber,
                a_building_name: $scope.propertyApaForm.aBuildingName,
                a_community_area: $scope.propertyApaForm.aCommunityArea,
                a_city: $scope.propertyApaForm.aCity,
                a_street: $scope.propertyApaForm.aStreet,
                a_parking_lot: $scope.propertyApaForm.aParkingLot,
                v_alias: $scope.propertyVilForm.vAlias,
                v_title_deed: $scope.propertyVilForm.vTitleDeed,
                v_dewa_number: $scope.propertyVilForm.vDewaNumber,
                v_unit_type: $scope.propertyVilForm.vUnitType,
                v_unit_number: $scope.propertyVilForm.vUnitNumber,
                v_community_area: $scope.propertyVilForm.vCommunityArea,
                v_city: $scope.propertyVilForm.vCity,
                v_street: $scope.propertyVilForm.vStreet
            }
        }
        apiService.savecontract.save(params).$promise.then(function (res) {
            if (res.status == "Success") {
                $scope.loader = false;
                toaster.pop("success", "Saved");
                var contract_id = res.contract_id;
                if (contract_id != undefined) {
                    $window.sessionStorage.setItem("contract_id", res.contract_id);
                }
                //$scope.propertyApaForm = {};
                //$scope.propertyApa.$setUntouched();
                //$scope.propertyApa.$setPristine();
                //$scope.propertyVilForm = {};
                //$scope.propertyVil.$setUntouched();
                //$scope.propertyVil.$setPristine();
            } else if (res.status = "error") {
                $scope.loader = false;
                toaster.pop("error", "Server error, please try later");
            }
        })
    };

    $scope.addTenancyTerms = function () {
        $scope.loader = true;
        var params = {
            "token": $window.sessionStorage.getItem("token"),
            "contract_id": $window.sessionStorage.getItem("contract_id"),
            "tenancyTerms": {
                lease_duration: $scope.tenancytForm.leaseDuration,
                start_date: $scope.tenancytForm.startDate,
                end_date: $scope.tenancytForm.endDate,
                rent_value: $scope.tenancytForm.rentValue,
                security_deposit: $scope.tenancytForm.securityDeposit,
                payment_terms: $scope.tenancytForm.paymentTerms,
                number_payment: $scope.tenancytForm.numberPayment,
                amount_dates: $scope.tenancytForm.amountsDates
            },
        }
        apiService.savecontract.save(params).$promise.then(function (res) {
            if (res.status == "Success") {
                $scope.loader = false;
                toaster.pop("success", "Saved");
                var contract_id = res.contract_id;
                if (contract_id != undefined) {
                    $window.sessionStorage.setItem("contract_id", res.contract_id);
                }
            } else if (res.status = "error") {
                $scope.loader = false;
                toaster.pop("error", "Server error, please try later");
            }
        })
    }

    $scope.addTnc = function () {
        $scope.loader = true;
        var params = {
            "token": $window.sessionStorage.getItem("token"),
            "contract_id": $window.sessionStorage.getItem("contract_id"),
            "termsAndConditions": {
                optional_terms: $scope.optionalTerms,
                language: $scope.lang
            },
        }
        apiService.savecontract.save(params).$promise.then(function (res) {
            if (res.status == "Success") {
                $scope.loader = false;
                toaster.pop("success", "Saved");
                var contract_id = res.contract_id;
                if (contract_id != undefined) {
                    $window.sessionStorage.setItem("contract_id", res.contract_id);
                }
            } else if (res.status = "error") {
                $scope.loader = false;
                toaster.pop("error", "Server error, please try later");
            }
        })
    }

    $scope.uploadFiles = function () {
        $scope.loader = true;
        var params = {
            "token": $window.sessionStorage.getItem("token"),
            "contract_id": $window.sessionStorage.getItem("contract_id"),
            "uploadDocuments": {
                title_deed: $scope.filesForm.titleDeed,
                dewa_bill: $scope.filesForm.dewaBill,
                tenant_id_front: $scope.filesForm.tIdFront,
                tenant_id_back: $scope.filesForm.tIdBack,
                tenant_passport_main: $scope.filesForm.tPassportMain,
                tenant_passport_visa: $scope.filesForm.tPassportVisa,
                tenant_company_license: $scope.filesForm.tCompanyLicense,
            }
        }
        apiService.uploadimages.save(params).$promise.then(function (res) {
            if (res.status == "Success") {
                toaster.pop("success", "Saved");
                var contract_id = res.contract_id;
                if (contract_id != undefined) {
                    $window.sessionStorage.setItem("contract_id", res.contract_id);
                }
                $scope.titleDeed = res.image_deed;
                $scope.dewaBill = res.image_dewa;
                $scope.tIdFront = res.emirates_front;
                $scope.tIdBack = res.emirates_back;
                $scope.tPassportMain = res.passport_main;
                $scope.tPassportVisa = res.passport_visa;
                $scope.tCompanyLicense = res.company_license;
                $scope.loader = false;
                $scope.uploadedfiles = res.images;
                angular.forEach(angular.element("input[type='file']"), function (inputElem) {
                    angular.element(inputElem).val(null);
                });
            } else if (res.status = "error") {
                $scope.loader = false;
                toaster.pop("error", "Server error, please try later");
                $scope.filesForm.titleDeed = null;
                $scope.filesForm.dewaBill = null;
                $scope.filesForm.idFront = null;
                $scope.filesForm.idBack = null;
                $scope.filesForm.passportMain = null;
                $scope.filesForm.passportVisa = null;
                $scope.filesForm.companyLicense = null;
            }
        })
    };

    $scope.delLandlord = function (landlordId) {
        $scope.loader = true;
        var params = {
            "token": $window.sessionStorage.getItem("token"),
            "contract_id": $window.sessionStorage.getItem("contract_id"),
            "landlord_id": landlordId
        }
        apiService.landlord.delete(params).$promise.then(function (res) {
            $scope.landlords = res.landlords;
            $scope.loader = false;
            toaster.pop("success", "Deleted");
            if (res.landlords) {
                if (res.landlords.length < 3) {
                    $scope.addAnotherLand = true;
                }
            }
        });
    }

    $scope.editlandlord = function (landlordId) {
        $scope.loader = true;
        var formTop = angular.element($('.form-top'));
        $document.scrollToElementAnimated(formTop, [1000]);
        var params = {
            "token": $window.sessionStorage.getItem("token"),
            "contract_id": $window.sessionStorage.getItem("contract_id"),
            "landlord_id": landlordId
        }
        $window.sessionStorage.setItem("landlord_id", landlordId);
        apiService.editlandlord.get(params).$promise.then(function (res) {
            $scope.singleLandlord = res.landlord;
            $scope.loader = false;
            $scope.noLandlordFound = true;
            $scope.landlordType = res.landlord.l_type;
            $scope.landlordIndForm.lFirstName = res.landlord.l_first_name;
            $scope.landlordIndForm.lLastName = res.landlord.l_last_name;
            $scope.landlordIndForm.lMobileNumber = res.landlord.l_mobile;
            $scope.landlordIndForm.lEmailAddress = res.landlord.l_email;
            $scope.landlordIndForm.lEmiratesId = res.landlord.l_emirates_id;
            $scope.landlordIndForm.lNationality = res.landlord.l_nationality;
            $scope.landlordIndForm.lPassportNumber = res.landlord.l_passport_no;
            $scope.landlordComForm.lCompanyName = res.landlord.l_company_name;
            $scope.landlordComForm.lEmiratesLicense = res.landlord.l_emirates_license_no;
            $scope.landlordComForm.lEstCard = res.landlord.l_establishment_card_no;
            $scope.landlordComForm.lIssuingAuthority = res.landlord.l_issuing_authority;
            $scope.landlordComForm.lPoBox = res.landlord.l_po_box;
            $scope.landlordComForm.lManagerFname = res.landlord.l_manager_first_name;
            $scope.landlordComForm.lManagerLname = res.landlord.l_manager_last_name;
            $scope.landlordComForm.lTelephoneNumber = res.landlord.l_company_phone;
            $scope.landlordComForm.lManagerMnumber = res.landlord.l_company_mobile;
            $scope.landlordComForm.lCompanyEmail = res.landlord.l_company_email;
            $scope.landlordComForm.lManagerEid = res.landlord.l_manager_emirates_id_no
            $scope.landlordComForm.lCompanyLicenseImage = res.landlord.l_company_license_image
        });
    }

    $scope.genPdf = function () {
        $scope.loader = true;
        var params = {
            "token": $window.sessionStorage.getItem("token"),
            "pdf": "yes",
            "contract_id": $window.sessionStorage.getItem("contract_id"),
        }
        apiService.savecontract.save(params).$promise.then(function (res) {
            if (res.status == "Success") {
                $scope.loader = false;
                toaster.pop("success", "Pdf generated and email sent");
            } else if (res.status = "error") {
                $scope.loader = false;
                toaster.pop("error", "Server Error");
            }
        })
    };
});

mainCtrl.controller("newContractCtrl", function ($scope, $state, $document, Upload, $http, $rootScope, $stateParams, toaster, $window, apiService, fileReader) {
    $window.sessionStorage.setItem("contract_id", null);
    $window.sessionStorage.setItem("landlord_id", "null");
    $scope.optionalTerms = [];
    $scope.toggleSelection = function toggleSelection(term) {
        var idx = $scope.optionalTerms.indexOf(term);
        if (idx > -1) {
            $scope.optionalTerms.splice(idx, 1);
        } else {
            $scope.optionalTerms.push(term);
        }
    };
    $scope.tenantIndForm = {};
    $scope.lang = {
        name: ' ',
    };
    $scope.tenantComForm = {};
    $scope.landlordIndForm = {};
    $scope.landlordComForm = {};
    $scope.propertyApaForm = {};
    $scope.propertyVilForm = {};
    $scope.tenancytForm = {};
    $scope.filesForm = {};
    $scope.tenantType = "individual";
    $scope.landlordType = "individual";
    $scope.propertyType = "apartment";
    $scope.landlordForm = true;

    $scope.addTenant = function () {
        $scope.loader = true;
        var params = {
            "token": $window.sessionStorage.getItem("token"),
            "contract_id": $window.sessionStorage.getItem("contract_id"),
            //"property"	: {
            //	"property_type"			:	"apartment",
            //},
            "tenant": {
                tenant_type: $scope.tenantType,
                t_first_name: $scope.tenantIndForm.tFirstName,
                t_last_name: $scope.tenantIndForm.tLastName,
                t_employee_name: $scope.tenantIndForm.tEmployeeName,
                t_job_position: $scope.tenantIndForm.tJobPosition,
                t_mobile_number: $scope.tenantIndForm.tMobileNumber,
                t_email_address: $scope.tenantIndForm.tEmailAddress,
                t_emirates_id: $scope.tenantIndForm.tEmiratesId,
                t_nationality: $scope.tenantIndForm.tNationality,
                t_passport_number: $scope.tenantIndForm.tPassportNumber,
                t_company_name: $scope.tenantComForm.tCompanyName,
                t_emirates_license: $scope.tenantComForm.tEmiratesLicense,
                t_est_card: $scope.tenantComForm.tEstCard,
                t_issuing_authority: $scope.tenantComForm.tIssuingAuthority,
                t_po_box: $scope.tenantComForm.tPoBox,
                t_manager_fname: $scope.tenantComForm.tManagerFname,
                t_manager_lname: $scope.tenantComForm.tManagerLname,
                t_telephone_number: $scope.tenantComForm.tTelephoneNumber,
                t_manager_mnumber: $scope.tenantComForm.tManagerMnumber,
                t_company_email: $scope.tenantComForm.tCompanyEmail,
                t_manager_eid: $scope.tenantComForm.tManagerEid
            }
        }
        apiService.savecontract.save(params).$promise.then(function (res) {
            if (res.status == "Success") {
                $scope.loader = false;
                toaster.pop("success", "Saved");
                var contract_id = res.contract_id;
                if (contract_id != undefined) {
                    $window.sessionStorage.setItem("contract_id", res.contract_id);
                }
                $scope.tenantComForm = {};
                $scope.tenantInd.$setUntouched();
                $scope.tenantInd.$setPristine();
                $scope.tenantIndForm = {};
                $scope.tenantCom.$setUntouched();
                $scope.tenantCom.$setPristine();
            } else if (res.status = "error") {
                $scope.loader = false;
                toaster.pop("error", "Server error, please try later");
            }
        })
    }

    $scope.addLandlord = function () {
        $scope.loader = true;
        var params = {
            "token": $window.sessionStorage.getItem("token"),
            "contract_id": $window.sessionStorage.getItem("contract_id"),
            //"tenant"	: {
            //	"tenant_type"			:	"individual",
            //},
            //"property"	: {
            //	"property_type"			:	"apartment",
            //},
            "landlord": {
                landlord_id: $window.sessionStorage.getItem("landlord_id"),
                landlord_type: $scope.landlordType,
                first_name: $scope.landlordIndForm.lFirstName,
                last_name: $scope.landlordIndForm.lLastName,
                mobile_number: $scope.landlordIndForm.lMobileNumber,
                email_address: $scope.landlordIndForm.lEmailAddress,
                emirates_id: $scope.landlordIndForm.lEmiratesId,
                nationality: $scope.landlordIndForm.lNationality,
                passport_number: $scope.landlordIndForm.lPassportNumber,
                emirate_id_front: $scope.landlordIndForm.lFrontImage,
                emirate_id_back: $scope.landlordIndForm.lBackImage,
                company_name: $scope.landlordComForm.lCompanyName,
                emirates_license: $scope.landlordComForm.lEmiratesLicense,
                est_card: $scope.landlordComForm.lEstCard,
                issuing_authority: $scope.landlordComForm.lIssuingAuthority,
                po_box: $scope.landlordComForm.lPoBox,
                manager_fname: $scope.landlordComForm.lManagerFname,
                manager_lname: $scope.landlordComForm.lManagerLname,
                telephone_number: $scope.landlordComForm.lTelephoneNumber,
                manager_mnumber: $scope.landlordComForm.lManagerMnumber,
                company_email: $scope.landlordComForm.lCompanyEmail,
                manager_eid: $scope.landlordComForm.lManagerEid,
                company_license_image: $scope.landlordComForm.lCompanyLicenseImage
            }
        };
        apiService.landlord.save(params).$promise.then(function (res) {
            $scope.loader = false;
            if (res.status == "Success") {
                toaster.pop("success", "Saved");
                $scope.landlords = res.landlord;
                if ($scope.landlordType == 'company') {
                    $scope.landlordForm = false;
                    var formTop = angular.element($('.form-top'));
                    $document.scrollToElementAnimated(formTop, [2000]);
                }
                $window.sessionStorage.setItem("contract_id", res.contract_id);
                $scope.landlordIndForm = {};
                $scope.landlordInd.$setUntouched();
                $scope.landlordInd.$setPristine();
                $scope.landlordComForm = {};
                $scope.landlordCom.$setUntouched();
                $scope.landlordCom.$setPristine();
                angular.forEach(angular.element("input[type='file']"), function (inputElem) {
                    angular.element(inputElem).val(null);
                });
            } else {
                toaster.pop("error", "Server error, please try later");
            }
            if (res.landlord) {
                if ($scope.landlords.length == 3) {
                    $scope.landlordForm = false;
                }
            }
        });
    };

    $scope.addProperty = function () {
        $scope.loader = true;
        var params = {
            "token": $window.sessionStorage.getItem("token"),
            "contract_id": $window.sessionStorage.getItem("contract_id"),
            //"tenant"	: {
            //	"tenant_type"			:	"individual",
            //},
            "property": {
                property_type: $scope.propertyType,
                a_alias: $scope.propertyApaForm.aAlias,
                a_title_deed: $scope.propertyApaForm.aTitleDeed,
                a_dewa_number: $scope.propertyApaForm.aDewaNumber,
                a_unit_type: $scope.propertyApaForm.aUnitType,
                a_unit_number: $scope.propertyApaForm.aUnitNumber,
                a_floor_number: $scope.propertyApaForm.aFloorNumber,
                a_building_name: $scope.propertyApaForm.aBuildingName,
                a_community_area: $scope.propertyApaForm.aCommunityArea,
                a_city: $scope.propertyApaForm.aCity,
                a_street: $scope.propertyApaForm.aStreet,
                a_parking_lot: $scope.propertyApaForm.aParkingLot,
                v_alias: $scope.propertyVilForm.vAlias,
                v_title_deed: $scope.propertyVilForm.vTitleDeed,
                v_dewa_number: $scope.propertyVilForm.vDewaNumber,
                v_unit_type: $scope.propertyVilForm.vUnitType,
                v_unit_number: $scope.propertyVilForm.vUnitNumber,
                v_floor_number: $scope.propertyVilForm.vFloorNumber,
                v_building_name: $scope.propertyVilForm.vBuildingName,
                v_community_area: $scope.propertyVilForm.vCommunityArea,
                v_city: $scope.propertyVilForm.vCity,
                v_street: $scope.propertyVilForm.vStreet
            }
        }
        apiService.savecontract.save(params).$promise.then(function (res) {
            if (res.status == "Success") {
                $scope.loader = false;
                toaster.pop("success", "Saved");
                var contract_id = res.contract_id;
                if (contract_id != undefined) {
                    $window.sessionStorage.setItem("contract_id", res.contract_id);
                }
                $scope.propertyApaForm = {};
                $scope.propertyApa.$setUntouched();
                $scope.propertyApa.$setPristine();
                $scope.propertyVilForm = {};
                $scope.propertyVil.$setUntouched();
                $scope.propertyVil.$setPristine();
            } else if (res.status = "error") {
                $scope.loader = false;
                toaster.pop("error", "Server error, please try later");
            }
        })
    };

    $scope.addTenancyTerms = function () {
        $scope.loader = true;
        var params = {
            "token": $window.sessionStorage.getItem("token"),
            "contract_id": $window.sessionStorage.getItem("contract_id"),
            //"tenant_type"			:	"individual",
            //"landlord_type"			:	"individual",
            //"property_type"			:	"apartment",
            "tenancyTerms": {
                lease_duration: $scope.tenancytForm.leaseDuration,
                start_date: $scope.tenancytForm.startDate,
                end_date: $scope.tenancytForm.endDate,
                rent_value: $scope.tenancytForm.rentValue,
                security_deposit: $scope.tenancytForm.securityDeposit,
                payment_terms: $scope.tenancytForm.paymentTerms,
                number_payment: $scope.tenancytForm.numberPayment,
                amount_dates: $scope.tenancytForm.amountsDates
            },
        }
        apiService.savecontract.save(params).$promise.then(function (res) {
            if (res.status == "Success") {
                $scope.loader = false;
                toaster.pop("success", "Saved");
                var contract_id = res.contract_id;
                if (contract_id != undefined) {
                    $window.sessionStorage.setItem("contract_id", res.contract_id);
                }
                $scope.tenancytForm = {};
                $scope.tenancyt.$setUntouched();
                $scope.tenancyt.$setPristine();
            } else if (res.status = "error") {
                $scope.loader = false;
                toaster.pop("error", "Server error, please try later");
            }
        })
    }

    $scope.addTnc = function () {
        $scope.loader = true;
        var params = {
            "token": $window.sessionStorage.getItem("token"),
            "contract_id": $window.sessionStorage.getItem("contract_id"),
            "termsAndConditions": {
                optional_terms: $scope.optionalTerms,
                language: $scope.lang
            },
        }
        apiService.savecontract.save(params).$promise.then(function (res) {
            if (res.status == "Success") {
                $scope.loader = false;
                toaster.pop("success", "Saved");
                var contract_id = res.contract_id;
                if (contract_id != undefined) {
                    $window.sessionStorage.setItem("contract_id", res.contract_id);
                }
            } else if (res.status = "error") {
                $scope.loader = false;
                toaster.pop("error", "Server error, please try later");
            }
        })
    }

    $scope.addLang = function () {

    }
    $scope.uploadFiles = function () {
        $scope.loader = true;
        var params = {
            "token": $window.sessionStorage.getItem("token"),
            "contract_id": $window.sessionStorage.getItem("contract_id"),
            "uploadDocuments": {
                title_deed: $scope.filesForm.titleDeed,
                dewa_bill: $scope.filesForm.dewaBill,
                tenant_id_front: $scope.filesForm.tIdFront,
                tenant_id_back: $scope.filesForm.tIdBack,
                tenant_passport_main: $scope.filesForm.tPassportMain,
                tenant_passport_visa: $scope.filesForm.tPassportVisa,
                tenant_company_license: $scope.filesForm.tCompanyLicense,
            }
        }
        apiService.uploadimages.save(params).$promise.then(function (res) {
            if (res.status == "Success") {
                toaster.pop("success", "Saved");
                var contract_id = res.contract_id;
                if (contract_id != undefined) {
                    $window.sessionStorage.setItem("contract_id", res.contract_id);
                }
                $scope.filesForm.titleDeed = null;
                $scope.filesForm.dewaBill = null;
                $scope.filesForm.idFront = null;
                $scope.filesForm.idBack = null;
                $scope.filesForm.passportMain = null;
                $scope.filesForm.passportVisa = null;
                $scope.filesForm.companyLicense = null;
                $scope.loader = false;
                $scope.uploadedfiles = res.images;
                angular.forEach(angular.element("input[type='file']"), function (inputElem) {
                    angular.element(inputElem).val(null);
                });
            } else if (res.status = "error") {
                $scope.loader = false;
                toaster.pop("error", "Server error, please try later");
                $scope.filesForm.titleDeed = null;
                $scope.filesForm.dewaBill = null;
                $scope.filesForm.idFront = null;
                $scope.filesForm.idBack = null;
                $scope.filesForm.passportMain = null;
                $scope.filesForm.passportVisa = null;
                $scope.filesForm.companyLicense = null;
            }
        })
    };

    $scope.delImage = function () {
        $scope.loader = true;
        //console.log($scope.filesForm.titleDeeeed);
        var params = {
            "token": $window.sessionStorage.getItem("token"),
            "contract_id": $window.sessionStorage.getItem("contract_id"),
            //"image_name"	: imgName
        }
        apiService.delimage.get(params).$promise.then(function (res) {
            if (res.status == "Success") {
                $scope.uploadedfiles = res.images;
                $scope.loader = false;
                toaster.pop("success", "Image Removed");
            } else {
                toaster.pop("error", "Server Error");
                $scope.loader = false;
            }
        })
    }

    $scope.genPdf = function () {
        $scope.loader = true;
        var params = {
            "token": $window.sessionStorage.getItem("token"),
            "pdf": "yes",
            "contract_id": $window.sessionStorage.getItem("contract_id"),
        }
        apiService.savecontract.save(params).$promise.then(function (res) {
            if (res.status == "Success") {
                $scope.loader = false;
                toaster.pop("success", "Pdf generated and email sent");
            } else if (res.status = "error") {
                $scope.loader = false;
                toaster.pop("error", "Server Error");
            }
        })
    };
});

mainCtrl.controller("checkoutCtrl", function ($scope, $rootScope, $window, $state, toaster, apiService, Auth) {
    $scope.couponSuccess = false;
    var coupenpParams = {
        coupon: "o"
    }
    apiService.checkout.get(coupenpParams).$promise.then(function (res) {
        $scope.token = res.token;
        $scope.amount = res.amount;
        $window.sessionStorage.setItem("actualamount", res.amount);
        $scope.currency = res.currency;
    })
    $scope.amount = "2500";
    $scope.currency = "AED";
    $scope.getDiscount = function () {
        $scope.loader = true;
        var params = {
            coupon: $scope.couponValue
        }
        apiService.checkout.get(params).$promise.then(function (res) {
            if (res.msg == "Invalid Coupon") {
                $scope.invalidCoupon = true;
                $scope.loader = false;
            } else if (res.status == "success") {
                $scope.loader = false;
                $window.sessionStorage.setItem("discountamount", res.amount);
                $scope.invalidCoupon = false;
                $scope.couponSuccess = true;
                $scope.amount = res.amount;
                $scope.currency = res.currency;
                $scope.token = res.token;
            }
        })
    }
});

mainCtrl.controller("checkoutFinal", function ($scope, $rootScope, $stateParams, $window, $state, toaster, apiService, Auth) {
    $scope.loader = true;
    if (!$window.sessionStorage.getItem("discountamount")) {
        $window.sessionStorage.setItem("discountamount", "null")
    }
    var params = {
        token: $window.sessionStorage.getItem("token"),
        contract_id: $window.sessionStorage.getItem("contract_id"),
        actual_amount: $window.sessionStorage.getItem("actualamount"),
        discount_amount: $window.sessionStorage.getItem("discountamount")
    }

    apiService.checkoutfinal.get(params).$promise.then(function (res) {
        $scope.loader = false;
        $state.go("contracts");
        $window.sessionStorage.removeItem("actualamount");
        $window.sessionStorage.removeItem("discountamount");
    })
});

mainCtrl.controller("history", function ($scope, $rootScope, $stateParams, $window, $state, toaster, apiService, Auth) {
    $scope.loader = true;
    var params = {
        reqTime: new Date().getTime(),
        token: $window.sessionStorage.getItem("token"),
    }
    apiService.history.get(params).$promise.then(function (res) {
        if (res.status == 'Success') {
            if (res.history) {
                $scope.userHistory = res.history;
                $scope.loader = false;
            } else {
                $scope.noHistory = true;
                $scope.loader = false;
            }
        } else {
            $scope.loader = false;
        }
    })
});