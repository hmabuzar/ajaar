(function (ng) {
    var ajaarApp = ng.module("ajaarApp", ["ui.router", "ngResource", "ui.bootstrap", "ngFileUpload", "ngMessages", "duScroll", "mainCtrl", "mainServices", "mainDirectives", "mainFilters", "ngAnimate", "toaster"]);

    ajaarApp.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
        //$httpProvider.interceptors.push("interceptor");
        $urlRouterProvider.otherwise("/login");

        $stateProvider
                .state("login", {
                    url: "/login",
                    templateUrl: "views/login.html",
                    controller: "loginCtrl",
                    module: "public"
                })
                .state("register", {
                    url: "/register",
                    templateUrl: "views/register.html",
                    controller: "regCtrl",
                    module: "public"
                })
                .state("forgotpass", {
                    url: "/forgotpass",
                    templateUrl: "views/forgotpass.html",
                    module: "public"
                })
                .state("resetpass", {
                    url: "/resetpass",
                    templateUrl: "views/resetpass.html",
                    module: "public"
                })
                .state("aboutus", {
                    url: "/about-us",
                    templateUrl: "views/about-us.html",
                    module: "public"
                })
                .state("newcontract", {
                    url: "/newcontract",
                    templateUrl: "views/newcontract.html",
                    controller: "newContractCtrl",
                    module: "private"
                })
                .state("contracts", {
                    url: "/contracts",
                    templateUrl: "views/contracts.html",
                    controller: "contracts",
                    module: "private"
                })
                .state("contractdetails", {
                    url: "/contracts/:id/details",
                    templateUrl: "views/contractdetails.html",
                    controller: "contractdetails",
                    module: "private"
                })
                .state("editcontract", {
                    url: "/contracts/:id/edit",
                    templateUrl: "views/editcontract.html",
                    controller: "editcontract",
                    module: "private"
                })
                .state("dubailaw", {
                    url: "/dubailaw",
                    templateUrl: "views/dubailaw.html",
                    module: "public"
                })
                .state("faq", {
                    url: "/faq",
                    templateUrl: "views/faq.html",
                    module: "public"
                })
                .state("privacypolicy", {
                    url: "/privacypolicy",
                    templateUrl: "views/privacypolicy.html",
                    module: "public"
                })
                .state("termsconditions", {
                    url: "/termsconditions",
                    templateUrl: "views/termsconditions.html",
                    module: "public"
                })
                .state("contactus", {
                    url: "/contactus",
                    templateUrl: "views/contactus.html",
                    module: "public"
                })
                .state("checkout", {
                    url: "/checkout",
                    templateUrl: "views/checkout.html",
                    controller: "checkoutCtrl",
                    module: "private"
                })
                .state("checkoutfinal", {
                    url: "/checkoutfinal",
                    templateUrl: "views/checkoutfinal.html",
                    controller: "checkoutFinal",
                    module: "private"
                })
                .state("history", {
                    url: "/history",
                    templateUrl: "views/history.html",
                    controller: "history",
                    module: "private"
                })
    });
    ajaarApp.run(function ($rootScope, $state, $templateCache, $location, Auth) {
        $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {
            $rootScope.loggedIn = Auth.isLogged();
            $rootScope.loader = true;
            if (toState.module === 'private' && !Auth.isLogged()) {
                e.preventDefault();
                $state.go('login');
                $rootScope.loader = false;
            }
            if ($rootScope.loggedIn && toState.url == "/login") {
                e.preventDefault();
                $state.go("newcontract");
                $rootScope.loader = false;
            }
            if ($rootScope.loggedIn && toState.url == "/register") {
                e.preventDefault();
                $state.go("newcontract");
                $rootScope.loader = false;
            }
        });
        $rootScope.$on("$stateChangeSuccess", function () {
            $rootScope.loader = false;
        });
        $rootScope.$on('$stateChangeStart', function (event, next, current) {
            if (typeof (current) !== 'undefined') {
                $templateCache.remove(current.templateUrl);
            }
        });
    });
})(angular);