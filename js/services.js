var mainServices = angular.module("mainServices", ["ngResource"]);

mainServices.factory("apiService", ["$resource", function ($resource) {
        return {
            login: $resource('https://ajaar.ae/admin/login'),
            register: $resource('https://ajaar.ae/admin/signup'),
            logout: $resource('https://ajaar.ae/admin/logout'),
            forgotpass: $resource('https://ajaar.ae/admin/forgotpass'),
            resetpass: $resource('https://ajaar.ae/admin/resetpass'),
            countries: $resource('https://ajaar.ae/admin/countries'),
            contracts: $resource('https://ajaar.ae/admin/contracts_list', {}, {
                query: {method: 'GET', isArray: false}
            }),
            contractdetails: $resource('https://ajaar.ae/admin/contracts'),
            savecontract: $resource('https://ajaar.ae/admin/add_contract'),
            loadterms: $resource('https://ajaar.ae/admin/terms'),
            uploadimages: $resource('https://ajaar.ae/admin/upload_images'),
            delimage: $resource('https://ajaar.ae/admin/del_image'),
            landlord: $resource('https://ajaar.ae/admin/landlord'),
            checkout: $resource('https://ajaar.ae/admin/checkout'),
            checkoutfinal: $resource('https://ajaar.ae/admin/finalize'),
            editlandlord: $resource('https://ajaar.ae/admin/edit_landlord'),
            contactus: $resource('https://ajaar.ae/admin/contactus'),
            history: $resource('https://ajaar.ae/admin/history_list')
        }
    }]);

mainServices.factory("Auth", function ($window) {
    return {
        isLogged: function () {
            var checkAuth = $window.sessionStorage.getItem("loggedIn");
            ;
            if (checkAuth) {
                return true;
            } else {
                return false;
            }
        },
        //getUser : function(){
        //	var loggedInUser = $window.sessionStorage.getItem("token");
        //	return loggedInUser;
        //}
    }
});