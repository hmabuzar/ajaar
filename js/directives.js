var mainDirectives	=	angular.module("mainDirectives", []);

mainDirectives.directive("pwCheck", function($compile){
	return {
		require : "ngModel",
		scope	:	true,
		link	:	function(scope, elem, attrs, ctrl){
			var firstpassword = "#" + attrs.pwCheck;
			elem.add(firstpassword).on("keyup", function(){
				scope.$apply(function(){
					var v = elem.val()===$(firstpassword).val();
					ctrl.$setValidity("pwMatch", v);
				});
			});
		}
	}
});

//mainDirectives.directive("ngFileSelect",function(){
//	return {
//		link: function($scope,el){
//			el.bind("change", function(e){
//				$scope.file = (e.srcElement || e.target).files[0];
//				$scope.getFile();
//			})
//		}
//	}
//});

mainDirectives.directive('customOnChange', function() {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			var onChangeHandler = scope.$eval(attrs.customOnChange);
			element.bind('change', onChangeHandler);
		}
	};
});

mainDirectives.directive("fileread", [function () {
	return {
		scope: {
			fileread: "="
		},
		link: function (scope, element, attributes) {
			element.bind("change", function (changeEvent) {
				var reader = new FileReader();
				reader.onload = function (loadEvent) {
					scope.$apply(function () {
						scope.fileread = loadEvent.target.result;
					});
				}
				reader.readAsDataURL(changeEvent.target.files[0]);
			});
		}
	}
}]);

mainDirectives.directive('checkFileSize',function(){
	return{
		link: function(scope, elem, attr, ctrl) {
			$(elem).bind('change', function() {
				var fileSize = this.files[0].size;
				var maxAllowed = 1*1024*1024;
				if(fileSize > maxAllowed){
					alert("Maximum 1 MB file allowed");
					return false;
				};
			});
		}
	}
});

mainDirectives.directive('validNumber', function() {
	return {
		require: '?ngModel',
		link: function(scope, element, attrs, ngModelCtrl) {
			if(!ngModelCtrl) {
				return;
			}

			ngModelCtrl.$parsers.push(function(val) {
				var clean = val.replace( /[^0-9]+/g, '');
				if (val !== clean) {
					ngModelCtrl.$setViewValue(clean);
					ngModelCtrl.$render();
				}
				return clean;
			});

			element.bind('keypress', function(event) {
				if(event.keyCode === 32) {
					event.preventDefault();
				}
			});
		}
	};
});
