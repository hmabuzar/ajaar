var mainFilters	=	angular.module("mainFilters", []);

mainFilters.filter("asDate", function () {
    return function (input) {
        input = input.replace(/(.+) (.+)/, "$1T$2Z");
        return new Date(input);
    }
});